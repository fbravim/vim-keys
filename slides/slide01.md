# VIM PARA APRENDER
# Um curso intensivo de VIM

## Apresentação

+ ▶ Blau Araujo
+ ▶ Matheus Martins

## Conteúdo

* Parte 1 : Por que aprender
* Parte 2 : Como aprender
* Parte 3 : Modos
* Parte 4 : Movimentação
* Parte 5 : Edição
* Parte 6 : Digitação
* Parte 7 : Arquivos, buffers e janelas
* Parte 8 : Busca e substituição
* Parte 9 : Autotexto e autocomplemento
* Parte 10: Configurações

## Material de apoio:

https://codeberg.org/blau_araujo/vim-keys

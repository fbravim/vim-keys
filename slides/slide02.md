# VIM PARA APRENDER
# Um curso intensivo de VIM

## Parte 1: Por que aprender

+ ▶ Meus motivos

- Disponível em qualquer instalação GNU/Linux (vi).

- Leve e responsivo.

- Facilmente configurável.

- Operado 100% pelo teclado.

- Foco na edição de textos (não na digitação).

# VIM PARA APRENDER
# Um curso intensivo de VIM

## Parte 1: Por que aprender

+ ▶ Perfeito para quem...

- Mora ou quer aprender a morar no terminal.

- Precisa editar arquivos remotamente.

- Usa equipamentos mais modestos.

- Edita muito código preexistente.

- Não resiste à tentação de editar durante
    a digitação.


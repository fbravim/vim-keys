# VIM PARA APRENDER
# Um curso intensivo de VIM

## Parte 2: Como aprender

* Utilize o VI/VIM.

* Utilize o VI/VIM, não algo supostamente parecido.

* Entenda o conceito de modos.

* Não tente decorar todas as teclas de uma vez!

* Comece pensando em ações corriqueiras, como:

: Inserir texto (escrever)..... i
: Sair do modo de inserção..... ESC
: Movimentação................. ← (h) ↓ (j) ↑ (k) → (l)
: Abrir arquivo................ ESC :e [ARQUIVO]
: Salvar....................... ESC :w [ARQUIVO]
: Salvar como.................. ESC :sav [ARQUIVO]
: Sair com arquivo salvo....... ESC :q
: Salvar e sair................ ESC :x
: Sair sem salvar.............. ESC :q!
: Selecionar texto............. ESC v <direções>
: Selecionar linhas............ ESC V <direções>
: Deletar/recortar seleção..... ESC v <direções> d
: Deletar/recortar uma linha... ESC dd
: Copiar uma linha............. ESC yy
: Copiar seleção............... ESC v <direções> y
: Colar........................ ESC P
